# delete file
from apiclient import errors
# ...

def delete_file(service, file_id):
  """Permanently delete a file, skipping the trash.

  Args:
    service: Drive API service instance.
    file_id: ID of the file to delete.
  """
  try:
    service.files().delete(fileId=file_id).execute()
  except errors.HttpError, error:
    print 'An error occurred: %s' % error